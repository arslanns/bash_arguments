# loops in bash.

for x in item item2 banana bread milk butter "French toast"
do
echo "you need to buy $x"
done

## Make a new list, with you favorit movies, and print it out using a loop.
for movie in titanic "kingsman" "Lion King"
do 
echo "my favorit movies: $movie"
done

## Make a list of crazy_x_landlords, use a loop to print them out and at then end tell me how many landlord where printed
## Hint: don't just hard code - use a variable to count. Initiating a counter.
## Start a counter OUTSIDE the loop
## in the loop's block, increment the counter. 

COUNTER=0
for landlord in Jack Tonny "The bearded one" "Cruella" Jonny
do 
echo "Example landord: $landlord"
COUNTER=$(expr $COUNTER + 1)
done

echo $COUNTER

## Make a new list of best restaurants, and print it out using a loop and in this format:
# > 1 - Sushi Samba
# > 2 - Franco Manca's
# > 3 - Anything with truffle

COUNTER=0
for restaurant in "Sushi Samba" "Franco Manca's" "Anything with truffle"
do
COUNTER=$(expr $COUNTER + 1)
echo "$COUNTER - $restaurant"
done


### Using arguments with for loops 
# $* means any number of argument passed to script
for arg in $*
do
echo "this was one of your arguments: $arg"
done